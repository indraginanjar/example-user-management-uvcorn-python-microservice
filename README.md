# User Management


## Pengenalan


Layanan API yang dibuat dengan dengan Python

    Pastikan sudah ter-install:

    - Python 3

    - PIP

## Menjalankan Layanan API

1. Pastikan MySQL telah terpasang dan berjalan

2. Buat database untuk menampung data

3. Set enviroment variable untuk menjadi acuan aplikasi menggunakan database, formatnya:

   Pada Bash:

   ```bash
   export DATABASE_URL="mysql+pymysql://nama_akun:password_akun@alamat_host_database/nama_database
   ```

   Pada PowerShell:

   ```powershell
   $env:DATABASE_URL="mysql+pymysql://nama_akun:password_akun@alamat_host_database/nama_database
   ```


4. Penuhi kebutuhan aplikasi dengan menjalankan perintah:

   ```
   pip install -r requirements.txt
   ```

5. Jalankan layanan API dengan perintah:

   ```bash
   uvicorn app.main:app --reload
   ```

    Perintah ini juga akan membuat tabel users pada database yang ditunjuk jika tabel tersebut belum ada.

6. Secara default layanan API bisa diakses pada http://localhost:8000/user

7. Dokumentasi swagger dari layanan API bisa diakses pada http://localhost:8000/docs

8. Halaman swagger juga menyediakan daftar API dalam format Json OpenAPI, yang bisa digunakan untuk di import kedalam aplikasi Postman dengan mudah. Json tersebut bisa di akses pada http://localhost:8000/openapi.json


## Menjalankan Dengan Docker Compose

1. Set paramater

   Buat berkas .env berisi:

   ```
   MYSQL_DATABASE=<nama_database_tempat_tabel_tabel_ditempatkan>
   MYSQL_ROOT_PASSWORD=<password_untuk_root_user_mysql>
   MYSQL_USER=<user_database_yang_digunakan>
   MYSQL_PASSWORD=<password_user_yang_digunakan>
   ```

   Atau bisa juga export/set environment variable berisi fied-field diatas.

2. Jalankan

   ```bash
   docker-compose up
   ```

## Testing

```
python -m pytest
```